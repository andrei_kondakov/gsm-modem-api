﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace GSMTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = Logger.Log;
            try
            {
                if (args.Length == 0)
                {
                    String portName1 = ConfigurationManager.AppSettings["portName1"];
                    int baudRate1 = Convert.ToInt32(ConfigurationManager.AppSettings["baudRate1"]);
                    SerialPortConnector serialPortConnector = new SerialPortConnector(portName1, baudRate1);
                    serialPortConnector.Start();
                }
                else if (args.Length == 1)
                {
                    Dictionary<string, SerialPortConnector> devices = new Dictionary<string, SerialPortConnector>();
                    string[] tests = File.ReadAllLines(args[0]);
                    foreach (string test in tests)
                    {
                        if (test.Contains("INIT"))
                        {
                            string[] options = test.Split(',');
                            if (options.Length == 4)
                            {
                                string deviceName = options[1];
                                string comPort = options[2];
                                int baudrate = Convert.ToInt32(options[3]);
                                devices.Add(deviceName, new SerialPortConnector(comPort, baudrate));
                                devices[deviceName].Init();
                            }
                            else
                            {
                                throw new Exception("The wrong test");
                            }
                        }
                        else if (test.Contains("SMS"))
                        {
                            string[] options = test.Split(',');
                            if (options.Length == 4)
                            {
                                string deviceName = options[1];
                                string phoneNumber = options[2]; // to 
                                string msg = options[3];
                                devices[deviceName].SendSMS(phoneNumber, msg);
                            }
                            else
                            {
                                throw new Exception("The wrong test");
                            }
                        }
                        else if (test.Contains("CALL"))
                        {
                            string[] options = test.Split(',');
                            if (options.Length == 4)
                            {
                                string deviceName = options[1];
                                string phoneNubmer = options[2];
                                int maxDuraiton = Convert.ToInt16(options[3]);
                                devices[deviceName].Call(phoneNubmer, maxDuraiton);
                            }
                            else
                            {
                                throw new Exception("The wrong test");
                            }
                        }
                        else continue;
                    }
                    // close all used ports
                    foreach (KeyValuePair<string, SerialPortConnector> device in devices)
                    {
                        device.Value.Close();
                    }
                }
                else
                {
                    throw new Exception("Usage: GSMTest <Path to test>(optional)");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                Console.WriteLine(ex);
                Console.ReadKey();
            }
            Console.ReadKey();
        }
    }
}
