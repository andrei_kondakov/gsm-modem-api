﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace GSMTest
{
    public class SerialPortConnector
    {
        /// <summary>
        /// Logger and reporter.
        /// </summary>
        private static Logger logger;
        /// <summary>
        /// Serial port device.
        /// </summary>
        private SerialPort port;
        /// <summary>
        /// Current (last) reponse of device.
        /// </summary>
        private string curResponse = String.Empty;

        /// <summary>
        /// Phone number of device.
        /// </summary>
        private string callerPhoneNumber;

        /// <summary>
        /// Instantiate a connector to COM-port.
        /// </summary>
        /// <param name="name">Name of COM-port (e.g COM1)</param>
        /// <param name="baudRate">Baudrate of COM-port</param>
        public SerialPortConnector(String name, int baudRate)
        {
            port = new SerialPort(name, baudRate);
            port.Parity = Parity.None;              // Specifies the parity bit for a SerialPort object.
            port.StopBits = StopBits.One;           // Specifies the number of stop bits used on the SerialPort object.
            port.WriteTimeout = 500;
            port.ReadTimeout = 500;
            port.DataBits = 8;
            port.Handshake = Handshake.None;
            port.RtsEnable = true;
            port.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
            logger = Logger.Log;
        }
        /// <summary>
        /// Handler for receiving data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            curResponse = (String)sp.ReadExisting().Clone();
            port.DiscardInBuffer();
            port.DiscardOutBuffer();
        }
        /// <summary>
        /// Waiting for correct answer.
        /// </summary>
        /// <param name="expectedResponse">Expected response</param>
        private void Wait(String expectedResponse)
        {
            int time = 0, timeout = 15499; // Timeout 15s for waiting respone
            while (!curResponse.Contains(expectedResponse))
            {
                Thread.Sleep(250);
                time += 250;
                if (time > timeout)
                {
                    logger.Error("Error: Timeout waiting the answer: " + expectedResponse);
                    throw new Exception("Error: Timeout waiting the answer: " + expectedResponse);
                }
            }
        }
        /// <summary>
        /// Initialization of GSM modem.
        /// </summary>
        public void Init()
        {
            try
            {
                logger.Info(String.Format("Start initialization modem {0}", port.PortName));
                this.Open();

                logger.Info("Sending \"AT\" command");
                port.Write("AT" + Environment.NewLine); Wait("OK");

                logger.Info("Sending \"ATZ\" command");
                port.Write("ATZ" + Environment.NewLine); Wait("OK");


                logger.Info("Sending \"AT^CURC=0\" command (not need diagnostic messages)");
                port.Write("AT^CURC=0" + Environment.NewLine); Wait("OK");

                logger.Info("Sending \"AT+CVHU=0\" command (enable hangup command using)");
                port.Write("AT+CVHU=0" + Environment.NewLine); Wait("OK");

                logger.Info("Sending \"AT+CMGF=1\" command");
                port.Write("AT+CMGF=1" + Environment.NewLine); Wait("OK");

                Thread.Sleep(200);
                logger.Info("Getting phone number of modem");
                port.Write("AT+CNUM" + Environment.NewLine);
                while (!curResponse.Contains("CNUM")) { }

                string[] responseParts = curResponse.Split(',');    // parsing response
                callerPhoneNumber = responseParts[1].Replace("\"", "");

                logger.Info("Phone number of sim in modem: " + callerPhoneNumber);
                Console.WriteLine("Phone number of sim in modem: {0}.", callerPhoneNumber);

                logger.Info("Get LAC, CellID and signal level (dBm)");
                // https://xinit.ru/bs/#!?mcc=250&mnc=02&lac=7799&cid=2648&networkType=gsm
                logger.Info("Sending \"AT+CREG=2\" command (enable network registration and location information unsolicited result code +CREG)");
                port.Write("ATE0" + Environment.NewLine); Wait("OK");
                port.Write("AT+CREG=2" + Environment.NewLine); Wait("OK");
                port.Write("AT+CREG?" + Environment.NewLine); Wait("CREG");
                responseParts = curResponse.Split(',');
                int lac, cellId, signalLevel;
                lac = Convert.ToInt32(responseParts[2].Trim(), 16);                     // Getting lac (hex) and convert to int (dec)
                cellId = Convert.ToInt32(responseParts[3].Split('\n')[0].Trim(), 16);   // Getting cellId (hex) and convert to int (dec)
                logger.Info(String.Format("{0}->LAC:{1} CellID:{2}", port.PortName, lac, cellId));

                port.Write("AT+CSQ" + Environment.NewLine); Wait("CSQ");
                responseParts = curResponse.Split(',');
                // http://www.gprsmodems.co.uk/images/csq1.pdf
                // http://m2msupport.net/m2msupport/signal-quality/
                // dBm = -113 + N * 2 
                signalLevel = -113 + Convert.ToInt16(Regex.Match(responseParts[0], @"\d+").Value) * 2;
                logger.Info(String.Format("{0}->Signal:{1}dBm", port.PortName, signalLevel));
                logger.Info("Initialization successful");
            }
            catch (Exception ex)
            {
                this.Close();
                logger.Error(ex.GetType().FullName + ": " + ex.Message);
                logger.Info("Trying init again");

                Console.WriteLine(ex.GetType().FullName + ": " + ex.Message);
                Console.WriteLine("Trying init again!");
                this.Init();
            }
        }
        /// <summary>
        /// Generate call from serial port device.
        /// </summary>
        /// <param name="calleePhoneNumber">Phone number to call</param>
        /// <param name="maxDuration">Maximum of talk time</param>
        public void Call(string calleePhoneNumber, int maxDuration)
        {
            if (maxDuration < 0)
            {
                throw new Exception("Error: bad input for method Call (maxDuration should be > 0)");
            }
            try
            {
                if (this.port.IsOpen)
                {
                    DateTime startTime = DateTime.Now;
                    int duration = -1, endStatus = -1, ccInfo = -1;
                    Console.WriteLine(">Call from {0} to {1} (maxDuration:{2}s)", callerPhoneNumber, calleePhoneNumber, maxDuration);
                    logger.Info(String.Format("Call from {0} to {1} (maxDuration:{2}s)", callerPhoneNumber, calleePhoneNumber, maxDuration));
                    bool connected = false;
                    Stopwatch timer = new Stopwatch();
                    string[] responseParts = null;
                    port.Write("ATDT" + calleePhoneNumber + ";" + Environment.NewLine);
                    Wait("CONF");
                    bool isEnd = false;
                    while (timer.Elapsed.TotalSeconds < maxDuration && !isEnd)
                    {
                        if (curResponse.Contains("CEND"))
                        {
                            isEnd = true;
                            responseParts = curResponse.Split(',');
                        }
                        else if (curResponse.Contains("CONN") && !connected)
                        {
                            timer.Start();
                            connected = true;
                            Console.WriteLine(">Connected, timer started");
                        }
                        else
                        {
                            Console.WriteLine(">{0}", timer.Elapsed.TotalSeconds);
                            Console.CursorLeft = 0;
                            Console.CursorTop--;
                        }
                    }
                    timer.Stop();
                    if (!isEnd)
                    {
                        port.Write("ATH" + Environment.NewLine);
                        while (!curResponse.Contains("CEND")) { }
                        responseParts = curResponse.Split(',');
                    }
                    duration = Convert.ToInt32(Regex.Match(responseParts[1], @"\d+").Value);
                    endStatus = Convert.ToInt32(Regex.Match(responseParts[2], @"\d+").Value);
                    ccInfo = Convert.ToInt32(Regex.Match(responseParts[3], @"\d+").Value);

                    logger.Info(String.Format("Connection ended (duration {0}s).", duration));
                    logger.Info(String.Format("End status: {0}", Huawei.EndStatus[endStatus]));
                    logger.Info(String.Format("Control Call Info: {0},", Huawei.CCInfo[ccInfo]));

                    if (connected)
                    {
                        logger.Report(String.Format("{0}; {1}; {2}; {3}; {4}", startTime.ToString(), duration, "call", callerPhoneNumber, calleePhoneNumber));
                    }
                    Console.WriteLine(">Connection ended (duration {0}s) call info: {1}.", duration, Huawei.CCInfo[ccInfo]);
                }
                else
                {
                    throw new Exception(port.PortName + " not opened");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                logger.Error("Called failed");
                logger.Error(ex.GetType() + " " + ex.Message);
            }
        }
        /// <summary>
        /// Send SMS-message.
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="message"></param>
        public void SendSMS(string phoneNumber, string message)
        {
            try
            {
                if (this.port.IsOpen)
                {
                    Console.WriteLine(">Sms from {0} to {1} (\"{2}\")", callerPhoneNumber, phoneNumber, message);
                    logger.Info(String.Format("Sms from {0} to {1} (\"{2}\")", callerPhoneNumber, phoneNumber, message));
                    DateTime time = DateTime.Now;
                    logger.Report(String.Format("{0}; {1}; {2}; {3}; {4}", time.ToString(), message, "sms", callerPhoneNumber, phoneNumber));
                    port.Write("AT+CMGS=\"" + phoneNumber + "\"" + Environment.NewLine);
                    Thread.Sleep(30);
                    port.Write(message + "\x1A"); //  CTRL+Z
                    Wait("OK");
                    Console.WriteLine(">Sms sent");
                    logger.Info("Sms sent");
                }
                else
                {
                    throw new Exception(port.PortName + " not opened");
                }
            }
            catch (Exception ex)
            {
                logger.Error("Sms send failed");
                logger.Error(ex.GetType() + " " + ex.Message);
            }
        }
        /// <summary>
        /// Start debug session.
        /// </summary>
        public void Start()
        {
            int input = 1;
            int[] validInputs = { 0, 1, 2 };
            this.Init();
            Console.WriteLine("Initialization completed.\nPress any key to continue");
            Console.ReadKey();
            while (input != 0)
            {
                Console.WriteLine("[1] Call");
                Console.WriteLine("[2] SMS");
                Console.WriteLine("[0] Exit");
                input = Convert.ToInt32(Console.ReadLine());
                if (!validInputs.Contains(input))
                {
                    Console.WriteLine("Error: Bad input");
                    continue;
                }
                else
                {
                    if (input == 1)
                    {
                        Console.Write("Enter a phone number to call (8XXXXXXXXXX): ");
                        String phoneNumber = Console.ReadLine().Trim();
                        this.Call(phoneNumber, 5);
                    }
                    if (input == 2)
                    {
                        Console.Write("Enter a phone number to send sms (8XXXXXXXXXX): ");
                        string phoneNumber = Console.ReadLine().Trim();
                        Console.Write("Enter message, please: ");
                        string message = Console.ReadLine();
                        SendSMS(phoneNumber, message);
                    }
                }
            }
            this.Close();
        }
        /// <summary>
        /// Open port.
        /// </summary>
        public void Open()
        {
            port.Open();
            Console.WriteLine("Port {0} opened with baudRate {1}.", port.PortName, port.BaudRate);
            logger.Info(String.Format("Port {0} opened with baudRate {1}.", port.PortName, port.BaudRate));
        }
        /// <summary>
        /// Close port.
        /// </summary>
        public void Close()
        {
            port.Close();
            Console.WriteLine("Port {0} closed.", port.PortName);
            logger.Info(String.Format("Port {0} closed.", port.PortName));
        }
    }
}
