﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace GSMTest
{
    /// <summary>
    /// Logger - to report about the call and sms
    /// and logging errors
    /// </summary>
    public class Logger
    {
        private static Logger log;
        private static string pathToLogFile;
        private static string pathToReportFile;

        private Logger()
        {
            string pathDirLog = Path.Combine(System.Environment.CurrentDirectory, "logs");
            if (!Directory.Exists(pathDirLog))
            {
                Directory.CreateDirectory(pathDirLog);
            }
            pathToLogFile = Path.Combine(pathDirLog, "log" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".txt");
            string pathDirReport = Path.Combine(System.Environment.CurrentDirectory, "reports");
            if (!Directory.Exists(pathDirReport))
            {
                Directory.CreateDirectory(pathDirReport);
            }
            pathToReportFile = Path.Combine(pathDirReport, "report" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".txt");
            Console.WriteLine("Path to report file: " +pathToReportFile);
            Console.WriteLine("Path to log file: " + pathToLogFile);
        }
        /// <summary>
        /// Singleton
        /// </summary>
        public static Logger Log
        {
            get
            {
                if (log == null)
                {
                    log = new Logger();
                }
                return log;
            }
        }
        private void Message(string type, string msg)
        {
            string logMessage = String.Format("[{0}][{1}]: {2}", DateTime.Now.ToString(), type.ToUpper(), msg);
            using (StreamWriter sw = File.AppendText(pathToLogFile))
            {
                sw.WriteLine(logMessage);
            }     
        }
        /// <summary>
        /// Write to log file a message with type "INFO"
        /// </summary>
        /// <param name="msg"></param>
        public void Info(string msg)
        {
            Message("info", msg);
        }
        /// <summary>
        /// Write to log file a message with type "ERROR"
        /// </summary>
        /// <param name="msg"></param>
        public void Error(string msg)
        {
            Message("error", msg);
        }
        /// <summary>
        /// Write to report file a message
        /// </summary>
        /// <param name="msg"></param>
        public void Report(string msg)
        {
            using (StreamWriter sw = File.AppendText(pathToReportFile))
            {
                sw.WriteLine(msg);
            }    
        }

    }
}
